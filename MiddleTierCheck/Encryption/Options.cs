﻿using CommandLine;

namespace Encryption
{
	class Options
	{
		[Option('e', "encrypt", HelpText = "Encrypt some text")]
		public string EncryptText { get; set; }

		[Option('d', "decrypt", HelpText = "Decrypt some text")]
		public string DecryptText { get; set; }

		[Option('k', "saltkey", HelpText = "SaltKey for decryption, must be exactly 16 characters long", Required =true)]
		public string SaltKey { get; set; }
	}
}
