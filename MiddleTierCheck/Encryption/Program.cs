﻿using System;
using CommandLine;
using EncryptionLibrary;

namespace Encryption
{
	class EcryptionCommandTool
	{
		static void Main(string[] args)
		{
			Options options = new Options();
			Parser parser = new Parser();

			if (parser.ParseArguments(args, options))
			{
				if (options.EncryptText != null)
				{
					Console.Write(EncryptionTool.ToEncrypt(options.EncryptText, options.SaltKey));
				}
				if (options.DecryptText != null)
				{
					Console.Write(EncryptionTool.ToDecrypt(options.DecryptText, options.SaltKey));
				}
			}
		}
	}
}
