﻿using System;
using System.Configuration;
using EncryptionLibrary;


namespace MiddleTierCheck
{
    class MiddleTierInfo
    {
        public string Protocol;
        public string Server;
        public int Port;
        public string Section;
        public string UserName;
        public string Password;
		public bool IsUp;
		public string Error;
		public string SaltKey;

        public MiddleTierInfo(string Section, string SaltKey)
        {
            this.Section = Section;
			this.SaltKey = SaltKey;
        }
        public void GetInfoFromConfig() 
        {
            try
            {
                ConfigSettings config = (ConfigSettings)ConfigurationManager.GetSection(this.Section);
                this.Port = config.Port;
                this.Protocol = config.Protocol;
                this.Server = config.Server;
                this.Password = EncryptionTool.ToDecrypt(config.Password, SaltKey);
                this.UserName = EncryptionTool.ToDecrypt(config.UserName, SaltKey);
            }
            catch (Exception e)
            {
				this.Error = e.ToString();
            }
        }
    }
}
