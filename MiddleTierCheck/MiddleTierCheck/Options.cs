﻿using CommandLine;

namespace MiddleTierCheck
{
	class Options
	{
		[Option('s', "server", HelpText = "ask for a server list")]
		public string Server { get; set; }

		[Option('l', "serverList")]
		public bool ServerList { get; set; }

		[Option('k', "saltKey", Required =true)]
		public string SaltKey { get; set; }
	}
}
