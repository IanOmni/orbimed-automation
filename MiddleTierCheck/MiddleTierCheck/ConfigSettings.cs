﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace MiddleTierCheck
{
    public class ConfigSettings : ConfigurationSection
    {
        public override bool IsReadOnly()
        {
            return false;
        }

        [ConfigurationProperty("Protocol", DefaultValue = "http")]
        public string Protocol
        {
            get { return (String)this["Protocol"]; }
            set { this["Protocol"] = value; }
        }

        [ConfigurationProperty("Server")]
        public string Server
        {
            get { return (String)this["Server"]; }
            set { this["Server"] = value; }
        }

        [ConfigurationProperty("Port")]
        public int Port
        {
            get { return (Int32)this["Port"]; }
            set { this["Port"] = value; }
        }

        [ConfigurationProperty("UserName")]
        public string UserName
        {
            get { return (String)this["UserName"]; }
            set { this["UserName"] = value; }
        }

        [ConfigurationProperty("Password")]
        public string Password
        {
            get { return (String)this["Password"]; }
            set { this["Password"] = value; }
        }
    }
}
