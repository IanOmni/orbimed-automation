﻿using System;
using System.Linq;
using Crd.Wsi;
using System.IO;
using CommandLine;
using System.Configuration;

namespace MiddleTierCheck
{
	class Program
	{
		static void Main(string[] args)
		{
			Options options = new Options();
			Parser parser = new Parser();
			string section = "";

			if (parser.ParseArguments(args, options))
			{

				if (options.ServerList == true)
				{
					var cfg = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
					var localSections = cfg.Sections.Cast<ConfigurationSection>().Where(s => s.SectionInformation.IsDeclared);
					foreach (var i in localSections)
					{
						Console.WriteLine(i.SectionInformation.SectionName);
					}
				}
				else if (options.Server != null)
				{
					section = options.Server;
					StreamWriter sw = new StreamWriter(section + "Log.txt");
					MiddleTierInfo MTinfo = new MiddleTierInfo(section, options.SaltKey);
					IsMiddleTierUp(MTinfo);
					if (MTinfo.IsUp) sw.WriteLine("UP");
					else
					{
						sw.WriteLine("DOWN");
						sw.WriteLine(MTinfo.Error);
					}
					sw.Close();
				}
			}
		}
		private static MiddleTierInfo IsMiddleTierUp(MiddleTierInfo MTinfo)
		{
			try
			{
				MTinfo.GetInfoFromConfig();
				ClientSession crdSession = new ClientSession(MTinfo.Protocol, MTinfo.Server, MTinfo.Port);
				LogonService login = crdSession.GetLogonService();
				login.logon(MTinfo.UserName, MTinfo.Password);
				login.getSessionKey();
				MTinfo.IsUp = true;
			}
			catch (Exception e)
			{
				MTinfo.IsUp = false;
				MTinfo.Error = e.ToString();
			}
			return MTinfo;
		}



	}
}
