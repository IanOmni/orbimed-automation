import psutil
import getopt, sys
import configparser
import os

def get_memory_usage():
    mem = psutil.virtual_memory()
    return str(mem.percent)

def get_cpu_usage(t):
    return str(psutil.cpu_percent(interval=t))

def get_disk_usage():
    return str(psutil.disk_usage('C:/')[3])

def performance(argv):
    config_return = {}
    if len(argv) > 0:
        return commands(argv)
    else:
        config_file_parser(config_return)
        for key in config_return:
            argv.append(config_return[key])
        return commands(argv)

def commands(argv):

    try:
        opts, args = getopt.getopt(argv, "mc:da", ["memory", "cpu=", "disk", "all"])
    except getopt.GetoptError:
        print("Needs Arguments, try -c, -m, -d, or -a")
        sys.exit(6)
    for opt, arg in opts:
        if opt in ('-a', '--all'):
            write_string = ('Memory: ' + get_memory_usage() + '\nCPU: ' + get_cpu_usage(1) + '\nDisk: ' + get_disk_usage())
        elif opt in ('-m', '--memory'):
            write_string = ('Memory: ' + get_memory_usage())
        elif opt in ('-c', '--cpu'):
            write_string = ('CPU: ' + get_cpu_usage(int(arg)))
        elif opt in ('-d', '--disk'):
            write_string = ('Disk: ' + get_disk_usage())
        return write_string

def config_file_parser(config_return):
    cp = configparser.ConfigParser()
    cp.read('config.ini')
    config_return['component'] = cp.get('Performance', 'component')
    config_return['time'] = cp.get('Performance', 'time')
    return config_return

def main():
    log = open(os.path.basename(__file__)[:-3]+'.txt', 'w')
    log.write(performance(sys.argv[1:]))
    log.close()

if __name__ == '__main__':
    main()
