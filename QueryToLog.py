import pyodbc
import getopt, sys
import configparser
import os
import subprocess

def config(argv):
    config_return = {}
    if len(argv) > 0:
        commands(argv, config_return)
        config_file_parser(config_return)
    else:
        config_file_parser(config_return)
    return config_return

def commands(argv, config_return):
    try:
        opts, args = getopt.getopt(argv, "q:hc:u:p:t:", ["query=", "connection=","user_name=","password=", "help", "trusted="])
    except getopt.GetoptError:
        print("Ask for help with -h")
        sys.exit(1)
    if len(opts)>0:
        for opt, arg in opts:
            if opt in ("-h", "--help"):
                print("""Commands:
                -q, --query
                -c, --connection
                -u, --user_name
                -p, --password
                -t, --trusted""")
            elif opt in ("-q", "--query"):
                config_return['sql'] = arg
            elif opt in ("-c", "--connection"):
                config_return['con_str'] = arg
            elif opt in ("-u", "--user_name"):
                config_return['user_name'] = arg
            elif opt in ("-p", "--password"):
                config_return['password'] = arg
            elif opt in ("-t", "--trusted"):
                config_return['trusted'] = arg
    else:
        print("Needs both query and connection string")

def decrypt(string):
    return subprocess.check_output(['Encryption.exe', '-k', 'omn-iVistaSaltKey','-d', string], universal_newlines=True)

def config_file_parser(config_return):
    cp = configparser.ConfigParser()
    cp.read('config.ini')
    if ('sql' in config_return) is False:
        config_return['sql'] = cp.get('Query', 'sql')
    if ('con_str' in config_return) is False:
        config_return['con_str'] = cp.get('ConnectionString', 'con_str')
    if ('user_name' in config_return) is False:
        config_return['user_name'] = decrypt(cp.get('ConnectionString', 'user_name'))
    if ('password'in config_return) is False:
        config_return['password'] = str(decrypt(cp.get('ConnectionString', 'password')))
    if ('trusted' in config_return) is False:
        config_return['trusted'] = cp.get('ConnectionString', 'trusted')

def con_str_gen(config_dict):
    con_str_return = config_dict['con_str']
    if config_dict['trusted'] == 'n':
        con_str_return = con_str_return  + " UID=" + config_dict['user_name'] + ";"
        con_str_return = con_str_return + " PWD=" + config_dict['password'] + ";"
    elif config_dict['trusted'] == 'y':
        con_str_return = con_str_return + " Trusted_Connection=yes;"
    else:
        print('Set -t, --trusted, or trusted= (in config.ini) to y or n')
    return con_str_return

def get_query_results(config_dict):
    cnxn = pyodbc.connect(con_str_gen(config_dict))
    cursor = cnxn.cursor()
    cursor.execute(config_dict['sql'])
    return cursor.fetchall()

def get_program_title():
    return os.path.basename(__file__)[:-3]

def main():
    config_dict = config(sys.argv[1:])
    results = get_query_results(config_dict)
    log = open(get_program_title()+'.txt', 'w')
    if results:
        for row in results:
            for col in row:
                log.write(str(col) + ' ')
            log.write('\n')
    log.close()

if __name__ == '__main__':
    main()
