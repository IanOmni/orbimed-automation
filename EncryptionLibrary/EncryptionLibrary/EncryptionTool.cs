﻿using System;
using System.Text;
using System.Security.Cryptography;

namespace EncryptionLibrary
{
	public class EncryptionTool
	{
		public static string ToEncrypt(string input, string saltkey)
		{
			return Encrypt(input, saltkey, false);
		}
		public static string ToDecrypt(string input, string saltkey)
		{
			return Decrypt(input, saltkey, false);
		}

		private static string Encrypt(string toEncrypt, string saltKey, bool useHashing)
		{
			byte[] keyArray;
			byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

			string key = saltKey;
			//System.Windows.Forms.MessageBox.Show(key);
			//If hashing use get hashcode regards to your key
			if (useHashing)
			{
				MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
				keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
				//Always release the resources and flush data
				// of the Cryptographic service provide. Best Practice

				hashmd5.Clear();
			}
			else
				keyArray = UTF8Encoding.UTF8.GetBytes(key);

			TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
			//set the secret key for the tripleDES algorithm
			tdes.Key = keyArray;
			//mode of operation. there are other 4 modes.
			//We choose ECB(Electronic code Book)
			tdes.Mode = CipherMode.ECB;
			//padding mode(if any extra byte added)

			tdes.Padding = PaddingMode.PKCS7;

			ICryptoTransform cTransform = tdes.CreateEncryptor();
			//transform the specified region of bytes array to resultArray
			byte[] resultArray =
			  cTransform.TransformFinalBlock(toEncryptArray, 0,
			  toEncryptArray.Length);
			//Release resources held by TripleDes Encryptor
			tdes.Clear();
			//Return the encrypted data into unreadable string format
			return Convert.ToBase64String(resultArray, 0, resultArray.Length);
		}

		private static string Decrypt(string toDcipherString, string saltKey, bool useHashing)
		{
			//System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
			//Logger logger = new Logger();
			//sw.Start();
			try
			{
				byte[] keyArray;
				//get the byte code of the string

				byte[] toEncryptArray = Convert.FromBase64String(toDcipherString);

				//Get your key from config file to open the lock!
				string key = saltKey;

				if (useHashing)
				{
					//if hashing was used get the hash code with regards to your key
					MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
					keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
					//release any resource held by the MD5CryptoServiceProvider

					hashmd5.Clear();
				}
				else
				{
					//if hashing was not implemented get the byte code of the key
					keyArray = UTF8Encoding.UTF8.GetBytes(key);
				}

				TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
				//set the secret key for the tripleDES algorithm
				tdes.Key = keyArray;
				//mode of operation. there are other 4 modes. 
				//We choose ECB(Electronic code Book)

				tdes.Mode = CipherMode.ECB;
				//padding mode(if any extra byte added)
				tdes.Padding = PaddingMode.PKCS7;

				ICryptoTransform cTransform = tdes.CreateDecryptor();
				byte[] resultArray = cTransform.TransformFinalBlock(
									 toEncryptArray, 0, toEncryptArray.Length);
				//Release resources held by TripleDes Encryptor                
				tdes.Clear();
				//if the text is garbage, then return an empty string
				string sOut = UTF8Encoding.UTF8.GetString(resultArray);
				if (sOut.Contains("") || sOut.Contains("") || sOut.Contains("?"))
					return string.Empty;
				//return the Clear decrypted TEXT
				return UTF8Encoding.UTF8.GetString(resultArray);
			}

			catch (Exception ex)
			{
				return string.Empty;
			}
			//sw.Stop();
			//logger.writeLine("\t Single Decryption Took: " + sw.Elapsed);

		}
	}
}
