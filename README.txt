------------------------------------------------------
Python scripts command line options, uses "config.ini" 
------------------------------------------------------
QueryToLog.py

>>Default state is to read from the configuration file (config.ini)
-q, --query 		>> SQL query, like "SELECT * FROM TS_ORDER"
-c, --connection 	>> Connection string for server, looks like :"Driver={SQL Server};Server=edb-omni-prod01; Database=CRD;"
-u, --user_name		>> Username to log into the server **NEEDS TO BE ENCRYPTED**
-p, --password		>> Password to log into the server **NEEDS TO BE ENCRYPTED**


PerformanceCheck

>>Default State is to read from the configuration file (config.ini)
-a, --all		>>Check CPU, Ram and Disk usage percentage
-m, --memory		>>Check memory percentage
-d, --disk		>>Check disk usage percentage
-c, --cpu		>>Check cpu usage percentage, number after for time to run check.


PerformanceCheckDB

>>Default State is to read from the configuration file (config.ini)
>>Needs run_ID, Will always write to database.
-a, --all		>>Check CPU, Ram and Disk usage percentage
-m, --memory		>>Check memory percentage
-d, --disk		>>Check disk usage percentage
-c, --cpu		>>Check cpu usage percentage, number after for time to run check.
-b, --database		>>Specify file path to SQLite3 database
-l, --log_file		>>Y or N to write to a log file with the same name as the program
-t, --table		>>Specify which table in database to write to
-i, --run_ID		>>A unique ID that is nessisary for each run.

----------------------------------------------------------
Executable scripts command line options, uses "app.config"
----------------------------------------------------------
MiddleTierCheck.exe

>>Needs server specified in command line and app.config and salt key
-s, --server 		>>Specifies server to check
-l, --serverList	>>Returns a list of servers
-k, --saltKey		>>Salt Key for encryption


Encryption.exe
>>Needs Salt Key to encrypt or decrypt
-k, --saltKey		>>Salt Key for encryption
-e, --encrypt		>>Command to encrypt text
-d, --decrypt		>>Command to decrypt text