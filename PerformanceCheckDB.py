import psutil
import getopt, sys
import configparser
import os
import sqlite3


def get_memory_usage():
    mem = psutil.virtual_memory()
    return str(mem.percent)

def get_cpu_usage(t):
    return str(psutil.cpu_percent(interval=t))

def get_disk_usage():
    return str(psutil.disk_usage('C:/')[3])

def config_dictionary(argv):
    config_return = {}
    if len(argv) > 0: #if there are arguments
        command_processor(argv, config_return)
        config_file_parser(config_return)
    else: #if there are not arguments
        config_file_parser(config_return)
    return config_return

def command_processor(argv, config_return):
    try:
        opts, args = getopt.getopt(argv, "mc:dab:l:t:i:", ["memory", "cpu=", "disk", "all", "database=", "log_file=", 'table=', 'run_ID='])
    except getopt.GetoptError:
        print("""Needs Arguments, try -c, -m, -d, -a, -b, -i,-t, or -l
                    log, table, database, run_ID, & cpu require additional information""")
        sys.exit(6)
    for opt, arg in opts:
        if opt in ('-a', '--all'):
            config_return['component'] = 'all'
        elif opt in ('-m', '--memory'):
            config_return['component'] = 'memory'
        elif opt in ('-c', '--cpu'):
            config_return['component'] = 'cpu'
            config_return['time'] = arg
        elif opt in ('-d', '--disk'):
            config_return['component'] = 'disk'
        if opt in ('-b', '--database'):
            config_return['database'] = arg
        if opt in ('-l', '--log_file'):
            config_return['log_file'] = arg
        if opt in ('-t', '--table'):
            config_return['table'] = arg
        if opt in ('-i', '--run_ID'):
            config_return['run_ID'] = arg

def write_to_log_file(config_return):
    log = open(os.path.basename(__file__)[:-3] + '.txt', 'w')
    if config_return['component'] == 'all':
        write_string = ('Memory: ' + get_memory_usage() + '\nCPU: ' + get_cpu_usage(1) + '\nDisk: ' + get_disk_usage())
        log.write(write_string)
    if config_return['component'] == 'memory':
        write_string = ('Memory: ' + get_memory_usage())
        log.write(write_string)
    if config_return['component'] == 'cpu':
        write_string = ('CPU: ' + get_cpu_usage(config_return['time']))
        log.write(write_string)
    if config_return['component'] == 'disk':
        write_string = ('Disk: ' + get_disk_usage())
        log.write(write_string)
    log.close()

def config_file_parser(config_return):
    cp = configparser.ConfigParser()
    cp.read('config.ini')
    if ('component' in config_return) is False:
        config_return['component'] = cp.get('Performance', 'component')
    if ('time' in config_return) is False:
        config_return['time'] = cp.get('Performance', 'time')
    if ('database' in config_return) is False:
        config_return['database'] = cp.get('Performance', 'database')
    if ('log' in config_return) is False:
        config_return['log_file']=cp.get('Performance', 'log_file')
    if ('table' in config_return) is False:
        config_return['table'] = cp.get('Performance', 'table')
    return config_return

def get_insert_info(config_return):
    database = config_return['database']
    table = config_return['table']
    run_ID = config_return['run_ID']
    return database, table, run_ID

def insert_into_db(config_return):
    database, table, run_ID = get_insert_info(config_return)
    try:
        conn = sqlite3.connect(database)
        cursor = conn.cursor()
        if config_return['component'] == 'all':
            cursor.execute('INSERT INTO {tn} VALUES ({rid}, datetime(CURRENT_TIMESTAMP, \'localtime\'), {cpu}, {ram}, {disk})'.format(tn=table, rid=run_ID, cpu=get_cpu_usage(1), ram=get_memory_usage(), disk=get_disk_usage()))
        elif config_return['component'] == 'memory':
            cursor.execute("INSERT INTO {tn} VALUES (RUN_ID, TIME_STAMP, RAM)({rid}, datetime(CURRENT_TIMESTAMP, \'localtime\'), {ram})".format(tn=table, rid=run_ID, ram=get_memory_usage()))
        elif config_return['component'] =='cpu':
            cursor.execute("INSERT INTO {tn} VALUES (RUN_ID, TIME_STAMP, CPU)({rid}, datetime(CURRENT_TIMESTAMP, \'localtime\'), {cpu})".format(tn=table, rid=run_ID, cpu=get_cpu_usage(config_return['time'])))
        elif config_return['component'] == 'disk':
            cursor.execute("INSERT INTO {tn} VALUES (RUN_ID, TIME_STAMP, DISK)({rid}, datetime(CURRENT_TIMESTAMP, \'localtime\'), {disk}))".format(tn=table, rid=run_ID, disk=get_disk_usage()))
        conn.commit()
        conn.close()

    except sqlite3.IntegrityError:
        log = open(os.path.basename(__file__)[:-3] + '.txt', 'w')
        log.write(str(sqlite3.IntegrityError))
        log.close()

def main():
    dictionary = config_dictionary(sys.argv[1:])
    if dictionary['log_file'] == 'y':
        write_to_log_file(dictionary)
    insert_into_db(dictionary)

if __name__ == '__main__':
    main()
